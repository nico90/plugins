<?php

/**
 * Plugin Name:       Tabs Categories
 * Description:       Categories Tabs, show products by categories!
 * Version:           1.0.0
 * Author:            Nicolas Andreoli
 * Author URI:        https://bitbucket.org/nico90/plugins/
 * Text Domain:       TabsCategories
 */

/*
 * Plugin constants
 */
if(!defined('TABS_PLUGIN_VERSION'))
    define('TABS_PLUGIN_VERSION', '1.0.0');
if(!defined('TABS_URL'))
    define('TABS_URL', plugin_dir_url( __FILE__ ));

/*
 * Main class
 */

class TabsCategories {

    /**
     * The security nonce
     *
     * @var string
     */
    private $_nonce = 'tabsCategories';

    /**
     * The option name
     *
     * @var string
     */
    private $option_name = 'categories_tabs';

    static $templateAdmin = '/templates/template.admin.php';

    const ICONS = 'https://file.myfontastic.com/M7FavKbZqHmJq5fAEDtZoV/icons.css';
    /**
     * tabs constructor.
     *
     * The main plugin actions registered for WordPress
     */
    public function __construct() {

        // Admin page calls
        add_action('admin_menu',                array($this,'addAdminMenu'));
        add_action('wp_enqueue_scripts',        array($this,'addFrontScripts'));
        add_action('admin_enqueue_scripts',     array($this,'addAdminScripts'));
        add_action('wp_ajax_updateOptionsAjax', array( $this, 'updateOptionsAjax' ) );
        add_action('wp_ajax_nopriv_updateOptionsAjax', array( $this, 'updateOptionsAjax' ));

        add_shortcode( 'categories_tabs_shortcode', array( $this, 'categories_tabs_shortcode') );

        $tabs = new stdClass();
        $tabs->categories = [];

        add_option($this->option_name, json_encode($tabs));

    }

    /**
     * Returns the saved options data as an array
     *
     * @return array
     */
    private function getData() {
        return json_decode(get_option($this->option_name, array()));
    }

    /**
     * Adds Admin Scripts for the Ajax call
     */
    public function addFrontScripts() {
        wp_enqueue_style('e2b-admin-ui-css','https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css',false,"1.9.0",false);
        wp_enqueue_style('icons',self::ICONS,false,"1.0",false);
        wp_enqueue_style('categories-tabs', TABS_URL. 'assets/css/front.css', false, 4.6);
        wp_enqueue_style('categories-tabs', TABS_URL. 'assets/css/jquery-ui.min.css', false, 1.0);
        wp_enqueue_style('structure-jquery', TABS_URL. 'assets/css/jquery-ui.structure.min.css', false, 1.0);
        wp_enqueue_style('theme-jquery', TABS_URL. 'assets/css/jquery-ui.theme.min.css', false, 1.0);
        wp_enqueue_script('categories-tabs-front', TABS_URL. 'assets/js/front.js', array('jquery'), 1.3);
        wp_enqueue_script('jquery-ui',TABS_URL. 'assets/js/jquery-ui.min.js');
        wp_localize_script('categories-tabs', 'categoriesTabs', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

    }

    /**
     * Adds Admin Scripts for the Ajax call
     */
    public function addAdminScripts() {
        wp_enqueue_style('e2b-admin-ui-css','https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css',false,"1.9.0",false);
        wp_enqueue_style('icons',self::ICONS,false,"1.0",false);
        wp_enqueue_style('categories-tabs', TABS_URL. 'assets/css/admin.css', false, 1.9);
        wp_enqueue_style('categories-tabs', TABS_URL. 'assets/css/jquery-ui.min.css', false, 1.0);
        wp_enqueue_style('structure-jquery', TABS_URL. 'assets/css/jquery-ui.structure.min.css', false, 1.0);
        wp_enqueue_style('theme-jquery', TABS_URL. 'assets/css/jquery-ui.theme.min.css', false, 1.0);

        wp_enqueue_script('categories-tabs', TABS_URL. 'assets/js/admin.js', array(), 4.2);
        wp_enqueue_script('categories-tabs-front', TABS_URL. 'assets/js/front.js', array(), 1.0);
        wp_enqueue_script('jquery-ui',TABS_URL. 'assets/js/jquery-ui.min.js');
        wp_localize_script( 'categories-tabs', 'categoriesTabs', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

    }

    /**
     * Adds the Tabs label to the WordPress Admin Sidebar Menu
     */
    public function addAdminMenu() {
        add_menu_page(
            __( 'Categories Tabs', 'tabsCategories' ),
            __( 'Categories Tabs', 'tabsCategories' ),
            'manage_options',
            'tabsCategories',
            array($this, 'adminLayout'),
            'dashicons-welcome-widgets-menus'
        );
    }

    /**
     * Get a Dashicon for a given status
     *
     * @param $valid boolean
     *
     * @return string
     */
    private function getStatusIcon($valid) {

        return ($valid) ? '<span class="dashicons dashicons-yes success-message"></span>' : '<span class="dashicons dashicons-no-alt error-message"></span>';

    }

    /**
     * Outputs the Admin Dashboard layout containing the form with all its options
     *
     * @return void
     *
     * data I will save
     *
     *
    {
    "tabs": [

     {
    "font": <CDN>
        "category": 1,
        "icon": "<url_file>"
        }]
    }
     */
    public function adminLayout() {

        $has_wc = (class_exists('WooCommerce'));

        if($has_wc) {
            ?>
            <h2><?php _e('Category tabs'); ?></h2>

            <?php
                $categories =  get_terms( 'product_cat', [
                    'orderby'    => 'name',
                    'order'      => 'asc',
                    'hide_empty' => false,
                ]);

                $options = $this->getData();
            ?>

            <div class="conteiner-categories">
                <div class="elements element-selected" data-max=5>
                    <h2>Catogories selected</h2>
                    <?php foreach ($options->categories as $optionCategory) : ?>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input categories-tabs" value="<?php echo $optionCategory->id ?>" checked data-name="<?php echo $optionCategory->name; ?>" data-icon="<?php echo $optionCategory->icon; ?>">
                            <label class="form-check-label" for="<?php echo $optionCategory->id ?>"><i id="<?php echo $optionCategory->id ?>" class="<?php echo $optionCategory->icon ?>"></i><?php echo $optionCategory->name; ?></label>
                            <div class="dashicons-before dashicons-admin-tools setup-icon"><br></div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="elements element-no-selected">
                    <h2>Catogories no selected</h2>
                    <?php
                    $categoriesSelected = [];
                    foreach ($categories as $category) :

                        foreach ($options->categories as $optioncategory) {
                            if($optioncategory->id == $category->term_id) {
                                $categoriesSelected[$category->term_id] = $category->term_id;
                            }
                        }

                        if(!in_array($category->term_id, $categoriesSelected)):
                            ?>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input categories-tabs" value="<?php echo $category->term_id ?>" data-name="<?php echo $category->name; ?>">
                                <label class="form-check-label" for="<?php echo $category->term_id ?>"><i id="<?php echo $category->term_id ?>" class=""></i><?php echo $category->name ?></label>
                                <div class="dashicons-before dashicons-admin-tools setup-icon"><br></div>
                            </div>
                        <?php
                        endif;

                    endforeach; ?>
                </div>

                <div id="myModal" class="my-modal">
                    <div class="container-icons">
                        <?php
                        foreach ($this->getIcons() as $icon):
                            ?>
                            <i class="<?php echo $icon ?>"></i>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="warning widget-area"></div>

                <button class="save-config">SAVE</button>
            </div>

    <?php

        } else {
            echo 'You should install WooCommerce :)';
        }

    }

    public function updateOptionsAjax() {
        $options = new stdClass();
        $options->categories = empty($_POST['categories']) ? [] : $_POST['categories'];
        update_option($this->option_name, json_encode($options));
        die();
    }

    private function getIcons() {
        $font_vars = file_get_contents(self::ICONS);

        $lines = explode("\n", $font_vars);

        foreach($lines as $line) {
            if(strpos($line, '.wtab-') !== FALSE)
                $matches[] = strtok( strtok($line, ':'), ".");

        }

        return $matches;

    }

    public function categories_tabs_shortcode() {
        ob_start();

        $options = $this->getData();
        foreach($options->categories as $category) {
            $categories[] = $category->id;
        }

    ?>
        <div class="products-container">
            <span id="tabs">
                <ul>
                    <?php
                    foreach($options->categories as $key => $option) :
                        echo '<li><a href=' . "#tabs-$key" . "> <i class=" . $option->icon . "></i><span class='name-category'>". $option->name . "</span></a></li>";
                    endforeach;
                    ?>
                </ul>
                <span class="title-mobile"></span>
                <?php foreach($options->categories as $key => $category) : ?>
                    <div class='tabs-animate' id="tabs-<?php echo $key ?>">
                        <?php
                        echo do_shortcode('[products limit="8" columns="4" category="'.$category->id.'"]');
                        ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php

        return ob_get_clean();
    }

}

$tab = new TabsCategories();

?>