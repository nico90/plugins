const mq = window.matchMedia( "(min-width: 768px)" );

jQuery(document).ready( function($){
    $( "#tabs" ).tabs({
        hide: { effect: "slide", duration: 500 },
        show: { effect: "slide", duration: 500 }
    }).addClass( "ui-tabs-vertical ui-helper-clearfix" );

    if (!mq.matches) {

        $('.title-mobile').html(
            $('.i-tabs-active a').find('span.name-category').html()
        );

        $('.ui-state-default a').click( () => {
            $('.title-mobile').html(
                $(this).find('span.name-category').html()
            );
        });
    }
});