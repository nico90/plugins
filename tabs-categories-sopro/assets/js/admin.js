jQuery(document).ready( function($){

    $(".element-selected").height(
        $(".element-no-selected").height()
    );

    $(".form-check").draggable({
        appendTo: "body",
        cursor: "move",
        helper: 'clone',
        revert: "invalid"
    });

    $(".element-no-selected").droppable({
        tolerance: "intersect",
        accept: ".form-check",
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        drop: function(event, ui) {

            $(this).append($(ui.draggable));
            $(ui.draggable).find("input").attr('checked',false);
            $('.warning').fadeOut();
        }
    });

    $(".element-selected").droppable({
        tolerance: "intersect",
        accept: ".form-check",
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        drop: function(event, ui) {
            if ($(this).children().length > $(this).data('max')) {
                $('.warning').fadeIn().html('You cannot select more than 5 categories');
                return false;
            }

            $(this).append($(ui.draggable));
            $(ui.draggable).find("input").attr('checked',true);
        }
    });

    $(".save-config").click( function(event) {
        event.preventDefault();
        let categories = [];

        $(".categories-tabs:checked").each(function (i) {
            categories[i] = {
                "id": $(this).val(),
                "name":$(this).data('name'),
                "icon":$(this).data('icon')
            };
        });

        $.ajax({
            url: categoriesTabs.ajaxurl,
            type: 'POST',
            data:{
                action: 'updateOptionsAjax',
                categories: categories
            },
            success: function( data ){
                location.reload();
            }
        });
    });

    let categoryId;

    window.onclick = function(event) {
        if (event.target == document.getElementById('myModal')) {
            document.getElementById('myModal').style.display = "none";
            categoryId = '';
        }
    };

    $(".setup-icon").click(function(){
        $('.my-modal').fadeIn('fast');

        categoryId =
            $(this).parent().find('input').val();
    });

    $('.my-modal i').on('click', function(){
        $('#' + categoryId).attr('class', $(this).attr('class'));

        $("input[value='"+ categoryId +"']").attr('data-icon', $(this).attr('class'));
    })




});
